#!/bin/bash

# Função para executar o teste com o endereço IP fornecido
execute_test() {
	ip_address="$1"
    
	# Limpa os arquivos de saída
	> velocidade-iperf.txt
	> velocidade-netperf.txt
	> Time-iperf.txt
	> Time-netperf.txt
	> Chamadas-iperf.txt
	> Chamadas-netperf.txt
    
	# Loop para executar o comando 10 vezes
	for i in {1..10}
	do
    	# Executa o comando e anexa a saída ao arquivo
    	iperf -c "$ip_address" >> velocidade-iperf.txt
    	{ time iperf -c "$ip_address" ; } 2>> Time-iperf.txt
    	{ strace -c iperf -c "$ip_address" ; } 2>> Chamadas-iperf.txt
    	netperf -H "$ip_address" >> velocidade-netperf.txt
    	{ time netperf -H "$ip_address" ; } 2>> Time-netperf.txt
    	{ strace -c netperf -H "$ip_address" ; } 2>> Chamadas-netperf.txt

   done
    
	# Ordena o conteúdo do arquivo e redireciona para um novo arquivo
	#sort -o teste-0.txt teste-0.txt
	#sort -o teste-1.txt teste-1.txt
	#sort -o teste-2.txt teste-2.txt
	#sort -o teste-3.txt teste-3.txt
	#sort -o teste-4.txt teste-4.txt
	#sort -o teste-5.txt teste-5.txt
    
	echo "Execução concluída. Resultados velocidade iperf salvos em velocidade-iperf.txt"
	echo "Execução concluída. Resultados tempo de execução iperf salvos em Time-iperf.txt"
	echo "Execução concluída. Resultados velocidade netperf salvos em velocidade-netperf.txt"
	echo "Execução concluída. Resultados tempo de execução netperf salvos em Time-netperf.txt"
	echo "Execução concluída. Resultados chamadas em tempo de execução iperf salvos em Chamadas-iperf.txt"
	echo "Execução concluída. Resultados chamadas em tempo de execução netperf salvos em Chamadas-netperf.txt"
}

# Função para criar a interface gráfica
create_gui() {
	ip_address=$(yad --title "Teste de Desempenho" \
                	--form --field="Endereço IP:" \
                	--button="Executar Teste":0 \
                	--button="Sair":1)

	button_clicked=$?

	if [ $button_clicked -eq 0 ]; then
    	ip_address=$(echo "$ip_address" | cut -d '|' -f 1)
    	execute_test "$ip_address"
	fi
}

create_gui

